package inc.minuth.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.Serializable;

import inc.minuth.fragment.callback.EventCallBack;
import inc.minuth.fragment.fragment.DetailDataFragment;
import inc.minuth.fragment.fragment.ListDataFragment;
import inc.minuth.fragment.model.Web;

public class MainActivity extends AppCompatActivity implements EventCallBack {

    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    Fragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager=getSupportFragmentManager();
        fragment=new ListDataFragment();
        replaceFragment(fragment);

    }

    @Override
    public void onShowDetail(Web web) {
        fragment=new DetailDataFragment();
        Bundle bundle=new Bundle();
        bundle.putSerializable("data",web);
        fragment.setArguments(bundle);
        replaceFragment(fragment);

    }
    private void replaceFragment(Fragment fragment)
    {
        transaction=fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.commit();
    }
    private void removeFragment(Fragment fragment)
    {
        transaction=fragmentManager.beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }
    @Override
    public void onBackPressed() {
        if(fragment instanceof DetailDataFragment)
        {
            removeFragment(fragment);
            fragment=new ListDataFragment();
            replaceFragment(fragment);
        }
        else {
            super.onBackPressed();
        }

    }
}
