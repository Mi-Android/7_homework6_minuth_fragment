package inc.minuth.fragment.model;

import java.io.Serializable;

public class Web implements Serializable
{

    private String name;
    private String webUrl;
    private String phone;
    private String email;
    private String address;
    private int imagePath;

    public Web() {
    }

    public Web(String name, String webUrl, String phone, String email, String address, int imagePath) {
        this.name = name;
        this.webUrl = webUrl;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }
}
