package inc.minuth.fragment.callback;

import inc.minuth.fragment.model.Web;

public interface EventCallBack {
    void onShowDetail(Web web);
}
