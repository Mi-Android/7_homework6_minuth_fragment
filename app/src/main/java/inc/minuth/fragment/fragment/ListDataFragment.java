package inc.minuth.fragment.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import inc.minuth.fragment.R;
import inc.minuth.fragment.adapter.WebAdapter;
import inc.minuth.fragment.model.Web;


public class ListDataFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_list_data, container, false);
        RecyclerView  recyclerView=view.findViewById(R.id.recyclerview);
        WebAdapter adapter=new WebAdapter(getActivity(),getListData());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        return view;
    }
    private List<Web> getListData()
    {
        List<Web>webList=new ArrayList<>();
        int imgs[]={R.drawable.a,R.drawable.aa,R.drawable.ff,R.drawable.fgff,R.drawable.r,R.drawable.rf,R.drawable.rrf,R.drawable.tff,R.drawable.w};

        for(int i:imgs)
        {
            webList.add(new Web("XXX","www.xxx.org.kh","0987654322","xxx@gmail.com","abcdefg",i));
        }
        return webList;
    }

}
