package inc.minuth.fragment.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import inc.minuth.fragment.R;
import inc.minuth.fragment.model.Web;


public class DetailDataFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_detail_data, container, false);
        Bundle bundle=getArguments();

        Web web=(Web) bundle.getSerializable("data");

        ImageView imgLogo=view.findViewById(R.id.imgView);
        ImageView imgSubLogo=view.findViewById(R.id.imgLogo);
        TextView tvName=view.findViewById(R.id.tvName);
        TextView tvPhone=view.findViewById(R.id.tvPhone);
        TextView tvEmail=view.findViewById(R.id.tvMail);
        TextView tvWebUrl=view.findViewById(R.id.tvWeb);
        TextView tvAddress=view.findViewById(R.id.tvAddress);

        imgLogo.setImageResource(web.getImagePath());
        imgSubLogo.setImageResource(web.getImagePath());
        tvAddress.setText(web.getAddress());
        tvEmail.setText(web.getEmail());
        tvName.setText(web.getName());
        tvWebUrl.setText(web.getWebUrl());
        tvPhone.setText(web.getPhone());

        return view;
    }




}
